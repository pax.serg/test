### Application code

1. Gradle code removed because this is maven project
2. CarsAPI removed - this project simple and doesn't need this class. In case we need few endpoints from different services we can create few API and POJO classes.
3. Feature names changed to more understandable (imho). It is not clear `he sees the results displayed for apple` - who is he and what is the result?
4. Added arguments to features to make them reusable.
5. Added parametrized tests to cover few similar cases.
6. Added .gitlab-ci.yml file to run tests on Gitlab.