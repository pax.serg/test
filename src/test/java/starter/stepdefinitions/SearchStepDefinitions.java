package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.internal.common.assertion.Assertion;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {
    private static final String BASE_URL = "https://waarkoop-server.herokuapp.com/";
    private static final String SEARCH_URL = BASE_URL + "api/v1/search/demo/";

    @When("search for {string}")
    public void searchFor(String product) {
        SerenityRest.given().get(SEARCH_URL + product);
    }

    @Then("response status code should be {int}")
    public void responseStatusCodeShouldBe(int expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(expectedStatusCode));
    }

    @Then("every response {} should contains text {string}")
    public void everyResponseFieldShouldContains(String fieldName, String text) {
        restAssuredThat(response -> response.body(fieldName, everyItem(containsStringIgnoringCase(text))));
    }

    @Then("response should contains {} {string}")
    public void responseShouldContainsField(String fieldName, String text) {
        restAssuredThat(response -> response.body(fieldName, hasItem(containsString(text))));
    }

    @Then("response have no result")
    public void responseHaveNoResult() {
        restAssuredThat(response -> {
            response.body("detail.error", equalTo(true));
            response.body("detail.message", equalTo("Not found"));
        });
    }

}