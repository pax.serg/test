Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Case for <product>
    When search for "<product>"
    Then response status code should be 200
    And response should contains title "<expected product name>"

    Examples:
      | product | expected product name            |
      | apple   | Crystal Clear Sparkling apple    |
      | orange  | Fanta Orange PET Fles 500 ml     |
      | pasta   | Melkan Pasta kaas geraspt        |
      | cola    | Coca-Cola Zero sugar 12 x 330 ml |

  Scenario: All result of apple contains apple
    When search for "apple"
    Then response status code should be 200
    And every response title should contains text "apple"

  Scenario: No result scenario
    When search for "car"
    Then response status code should be 404
    And response have no result